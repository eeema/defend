1- MainProject directory contains:
	i-  The source code for the Domain Model schema.
	ii- An export Archive file for the entire project.
2- Runtime-EclipseApplication directory contains:
	i-  The user runtime view diagram.
	ii- An export Archive file for the entire runtime application. 
3- ScreenShots direcctory contains a screenshot of the final output of the project.
4- The project uses Eclipse/Sirius: ObeoDesigner-Community-11.1-win32.win32.x86_64 / obeodesigner to develop this task.
5- Java version: jre-8u201-windows-x64