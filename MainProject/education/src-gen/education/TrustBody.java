/**
 */
package education;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trust Body</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see education.EducationPackage#getTrustBody()
 * @model
 * @generated
 */
public interface TrustBody extends assuranceTask {
} // TrustBody
