/**
 */
package education;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see education.EducationFactory
 * @model kind="package"
 * @generated
 */
public interface EducationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "education";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/education";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "education";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EducationPackage eINSTANCE = education.impl.EducationPackageImpl.init();

	/**
	 * The meta object id for the '{@link education.impl.BodyImpl <em>Body</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see education.impl.BodyImpl
	 * @see education.impl.EducationPackageImpl#getBody()
	 * @generated
	 */
	int BODY = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Body</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY__BODY = 1;

	/**
	 * The number of structural features of the '<em>Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BODY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link education.impl.assuranceTaskImpl <em>assurance Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see education.impl.assuranceTaskImpl
	 * @see education.impl.EducationPackageImpl#getassuranceTask()
	 * @generated
	 */
	int ASSURANCE_TASK = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_TASK__NAME = BODY__NAME;

	/**
	 * The feature id for the '<em><b>Body</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_TASK__BODY = BODY__BODY;

	/**
	 * The number of structural features of the '<em>assurance Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_TASK_FEATURE_COUNT = BODY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>assurance Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSURANCE_TASK_OPERATION_COUNT = BODY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link education.impl.TrustBodyImpl <em>Trust Body</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see education.impl.TrustBodyImpl
	 * @see education.impl.EducationPackageImpl#getTrustBody()
	 * @generated
	 */
	int TRUST_BODY = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUST_BODY__NAME = ASSURANCE_TASK__NAME;

	/**
	 * The feature id for the '<em><b>Body</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUST_BODY__BODY = ASSURANCE_TASK__BODY;

	/**
	 * The number of structural features of the '<em>Trust Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUST_BODY_FEATURE_COUNT = ASSURANCE_TASK_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Trust Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRUST_BODY_OPERATION_COUNT = ASSURANCE_TASK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link education.impl.EduBodyImpl <em>Edu Body</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see education.impl.EduBodyImpl
	 * @see education.impl.EducationPackageImpl#getEduBody()
	 * @generated
	 */
	int EDU_BODY = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDU_BODY__NAME = ASSURANCE_TASK__NAME;

	/**
	 * The feature id for the '<em><b>Body</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDU_BODY__BODY = ASSURANCE_TASK__BODY;

	/**
	 * The number of structural features of the '<em>Edu Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDU_BODY_FEATURE_COUNT = ASSURANCE_TASK_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Edu Body</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDU_BODY_OPERATION_COUNT = ASSURANCE_TASK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link education.impl.AllBodiesImpl <em>All Bodies</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see education.impl.AllBodiesImpl
	 * @see education.impl.EducationPackageImpl#getAllBodies()
	 * @generated
	 */
	int ALL_BODIES = 4;

	/**
	 * The feature id for the '<em><b>Body</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALL_BODIES__BODY = 0;

	/**
	 * The number of structural features of the '<em>All Bodies</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALL_BODIES_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>All Bodies</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALL_BODIES_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link education.Body <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Body</em>'.
	 * @see education.Body
	 * @generated
	 */
	EClass getBody();

	/**
	 * Returns the meta object for the attribute '{@link education.Body#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see education.Body#getName()
	 * @see #getBody()
	 * @generated
	 */
	EAttribute getBody_Name();

	/**
	 * Returns the meta object for the reference '{@link education.Body#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Body</em>'.
	 * @see education.Body#getBody()
	 * @see #getBody()
	 * @generated
	 */
	EReference getBody_Body();

	/**
	 * Returns the meta object for class '{@link education.TrustBody <em>Trust Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Trust Body</em>'.
	 * @see education.TrustBody
	 * @generated
	 */
	EClass getTrustBody();

	/**
	 * Returns the meta object for class '{@link education.EduBody <em>Edu Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Edu Body</em>'.
	 * @see education.EduBody
	 * @generated
	 */
	EClass getEduBody();

	/**
	 * Returns the meta object for class '{@link education.assuranceTask <em>assurance Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>assurance Task</em>'.
	 * @see education.assuranceTask
	 * @generated
	 */
	EClass getassuranceTask();

	/**
	 * Returns the meta object for class '{@link education.AllBodies <em>All Bodies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>All Bodies</em>'.
	 * @see education.AllBodies
	 * @generated
	 */
	EClass getAllBodies();

	/**
	 * Returns the meta object for the containment reference list '{@link education.AllBodies#getBody <em>Body</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Body</em>'.
	 * @see education.AllBodies#getBody()
	 * @see #getAllBodies()
	 * @generated
	 */
	EReference getAllBodies_Body();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EducationFactory getEducationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link education.impl.BodyImpl <em>Body</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see education.impl.BodyImpl
		 * @see education.impl.EducationPackageImpl#getBody()
		 * @generated
		 */
		EClass BODY = eINSTANCE.getBody();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BODY__NAME = eINSTANCE.getBody_Name();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BODY__BODY = eINSTANCE.getBody_Body();

		/**
		 * The meta object literal for the '{@link education.impl.TrustBodyImpl <em>Trust Body</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see education.impl.TrustBodyImpl
		 * @see education.impl.EducationPackageImpl#getTrustBody()
		 * @generated
		 */
		EClass TRUST_BODY = eINSTANCE.getTrustBody();

		/**
		 * The meta object literal for the '{@link education.impl.EduBodyImpl <em>Edu Body</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see education.impl.EduBodyImpl
		 * @see education.impl.EducationPackageImpl#getEduBody()
		 * @generated
		 */
		EClass EDU_BODY = eINSTANCE.getEduBody();

		/**
		 * The meta object literal for the '{@link education.impl.assuranceTaskImpl <em>assurance Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see education.impl.assuranceTaskImpl
		 * @see education.impl.EducationPackageImpl#getassuranceTask()
		 * @generated
		 */
		EClass ASSURANCE_TASK = eINSTANCE.getassuranceTask();

		/**
		 * The meta object literal for the '{@link education.impl.AllBodiesImpl <em>All Bodies</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see education.impl.AllBodiesImpl
		 * @see education.impl.EducationPackageImpl#getAllBodies()
		 * @generated
		 */
		EClass ALL_BODIES = eINSTANCE.getAllBodies();

		/**
		 * The meta object literal for the '<em><b>Body</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALL_BODIES__BODY = eINSTANCE.getAllBodies_Body();

	}

} //EducationPackage
