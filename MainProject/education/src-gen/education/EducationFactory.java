/**
 */
package education;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see education.EducationPackage
 * @generated
 */
public interface EducationFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EducationFactory eINSTANCE = education.impl.EducationFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Body</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Body</em>'.
	 * @generated
	 */
	Body createBody();

	/**
	 * Returns a new object of class '<em>Trust Body</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trust Body</em>'.
	 * @generated
	 */
	TrustBody createTrustBody();

	/**
	 * Returns a new object of class '<em>Edu Body</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Edu Body</em>'.
	 * @generated
	 */
	EduBody createEduBody();

	/**
	 * Returns a new object of class '<em>assurance Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>assurance Task</em>'.
	 * @generated
	 */
	assuranceTask createassuranceTask();

	/**
	 * Returns a new object of class '<em>All Bodies</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>All Bodies</em>'.
	 * @generated
	 */
	AllBodies createAllBodies();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	EducationPackage getEducationPackage();

} //EducationFactory
