/**
 */
package education;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>All Bodies</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link education.AllBodies#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see education.EducationPackage#getAllBodies()
 * @model
 * @generated
 */
public interface AllBodies extends EObject {
	/**
	 * Returns the value of the '<em><b>Body</b></em>' containment reference list.
	 * The list contents are of type {@link education.Body}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body</em>' containment reference list.
	 * @see education.EducationPackage#getAllBodies_Body()
	 * @model containment="true"
	 * @generated
	 */
	EList<Body> getBody();

} // AllBodies
