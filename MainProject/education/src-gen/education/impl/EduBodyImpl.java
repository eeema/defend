/**
 */
package education.impl;

import education.EduBody;
import education.EducationPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Edu Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EduBodyImpl extends assuranceTaskImpl implements EduBody {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EduBodyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EducationPackage.Literals.EDU_BODY;
	}

} //EduBodyImpl
