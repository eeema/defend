/**
 */
package education.impl;

import education.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EducationFactoryImpl extends EFactoryImpl implements EducationFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EducationFactory init() {
		try {
			EducationFactory theEducationFactory = (EducationFactory) EPackage.Registry.INSTANCE
					.getEFactory(EducationPackage.eNS_URI);
			if (theEducationFactory != null) {
				return theEducationFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new EducationFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EducationFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case EducationPackage.BODY:
			return createBody();
		case EducationPackage.TRUST_BODY:
			return createTrustBody();
		case EducationPackage.EDU_BODY:
			return createEduBody();
		case EducationPackage.ASSURANCE_TASK:
			return createassuranceTask();
		case EducationPackage.ALL_BODIES:
			return createAllBodies();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Body createBody() {
		BodyImpl body = new BodyImpl();
		return body;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TrustBody createTrustBody() {
		TrustBodyImpl trustBody = new TrustBodyImpl();
		return trustBody;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EduBody createEduBody() {
		EduBodyImpl eduBody = new EduBodyImpl();
		return eduBody;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public assuranceTask createassuranceTask() {
		assuranceTaskImpl assuranceTask = new assuranceTaskImpl();
		return assuranceTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AllBodies createAllBodies() {
		AllBodiesImpl allBodies = new AllBodiesImpl();
		return allBodies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EducationPackage getEducationPackage() {
		return (EducationPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static EducationPackage getPackage() {
		return EducationPackage.eINSTANCE;
	}

} //EducationFactoryImpl
