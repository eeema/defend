/**
 */
package education.impl;

import education.EducationPackage;
import education.TrustBody;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trust Body</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TrustBodyImpl extends assuranceTaskImpl implements TrustBody {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TrustBodyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EducationPackage.Literals.TRUST_BODY;
	}

} //TrustBodyImpl
