/**
 */
package education.impl;

import education.EducationPackage;
import education.assuranceTask;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>assurance Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class assuranceTaskImpl extends BodyImpl implements assuranceTask {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected assuranceTaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EducationPackage.Literals.ASSURANCE_TASK;
	}

} //assuranceTaskImpl
